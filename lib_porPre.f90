!=======================================================================================================================
!    Portable Precision to use for each type of variable in the whole program to make it portable
!=======================================================================================================================
MODULE PorPre
    ! use, intrinsic :: iso_fortran_env, only: error_unit, input_unit, output_unit


    ! Precision for logical numbers
    ! --------------------------------------------------------------------
    ! integer, parameter :: sl = SELECTED_LOGICAL_KIND(2)     ! Do not exist yet and not very useful to me now
    ! integer, parameter :: dl =
    ! integer, parameter :: ql =


    ! Precision for integer numbers
    ! --------------------------------------------------------------------
    ! SELECTED_REAL_KIND(R) This gives the kind number to have range of 10^-R to 10^R
    integer, parameter :: vsi  = SELECTED_INT_KIND(2)    ! 1 BYTE : -127 to 127                          INT1
    integer, parameter :: si   = SELECTED_INT_KIND(4)    ! 2 BYTE : -32,768 to 32,767                    INT2
    integer, parameter :: di   = SELECTED_INT_KIND(9)    ! 4 BYTE : -2,147,483,648 to 2,147,483,647      INT4
    integer, parameter :: qi   = SELECTED_INT_KIND(18)   ! 8 BYTE : -9,223,372,036,854,775,808 to -...   INT8
    integer, parameter :: Dvsi = 2+4                     ! Digits used by this kind
    integer, parameter :: Dsi  = 4+4                     ! Digits used by this kind
    integer, parameter :: Ddi  = 9+4                     ! Digits used by this kind
    integer, parameter :: Dqi  = 18+4                    ! Digits used by this kind

    ! Precision for floating numbers
    ! --------------------------------------------------------------------
    ! SELECTED_REAL_KIND(P, R) This gives at least P decimal places and range of 10^-R to 10^R
    integer, parameter :: sr  = SELECTED_REAL_KIND(6, 37)       ! REAL32   bits == real(kind=4) :: float32 4 BYTE
    integer, parameter :: dr  = SELECTED_REAL_KIND(15, 307)     ! REAL64   bits                            8 BYTE
    ! integer, parameter :: qr  = SELECTED_REAL_KIND(33, 4931)    ! REAL128  bits      	                   16 BYTE
    
    ! The following lines are here for the preprocessor to check if the macro __PGI is defined.
    ! If that's the case it means we are compiling using a PGI compiler, and it then initialize the variable qr with 
    ! SELECTE_REAL_KIND(15,307) (limit value of PGI compiler) ; Else, the value given to qr 
    ! is SELECTED_REAL_KIND(33,4931) which is important in theory is conform to norm IEEE754.
#ifdef __PGI
    integer, parameter :: qr = SELECTED_REAL_KIND(15, 307)
#else
    integer, parameter :: qr = SELECTED_REAL_KIND(33, 4931)
#endif

    integer, parameter :: Dsr = 6+2+9+4                         ! Digits used by this kind 9= 1 Digits before decimals plus the dot plus E+10 plus 3 space
    integer, parameter :: Ddr = 15+9+4                          ! Digits used by this kind
    integer, parameter :: Dqr = 33+9+4                          ! Digits used by this kind
    ! character(*), parameter :: FR16P = '(E42.33E4)'           !< Output format for kind=R16P real.
    ! integer(I_P), parameter :: BII_P = bit_size(MaxI_P)       !< Number of bits of kind=I_P integer.
    real(dr),   parameter :: MaxDr = huge(1._dr)        !< Maximum value of kind=R16P real.
    integer(di), parameter :: ByDr = sizeof(MaxDr) !< Number of bytes of kind=I8P integer.
    ! real(R4P),    parameter :: ZeroR4  = spacing(1._R4P) ! nearest(1._R4P, 1._R4P) - nearest(1._R4P,-1._R4P) !< Smallest representable difference of kind=R4P real.
    ! real(R4P),    parameter :: smallR4P = tiny(1._R4P )       !< Smallest representable value of kind=R4P real.
    ! real(R16P),   parameter :: MinR16P = -huge(1._R16P)       !< Minimum value of kind=R16P real.


    ! Precision for Complex numbers
    ! --------------------------------------------------------------------
    integer, parameter :: sc = sr           ! use the real kind
    integer, parameter :: dc = dr
    integer, parameter :: qc = qr
    integer, parameter :: Dsc = 2*Dsr+9     ! use the real digits plus the wrapping brackets
    integer, parameter :: Ddc = 2*Ddr+9
    integer, parameter :: Dqc = 2*Dqr+9


    ! Precision for string variables
    ! --------------------------------------------------------------------
    integer, parameter :: ss = SELECTED_CHAR_KIND('ascii')          ! ascii
    integer, parameter :: ds = SELECTED_CHAR_KIND('ISO_10646')      ! unicode utf8  ! Not supported by Intel ifort yet
    ! integer, parameter :: qs = SELECTED_CHAR_KIND('')               !
    ! parameter for string lenght
    integer(kind=si), public, parameter :: strl     = 16            ! very short string lenght
    integer(kind=si), public, parameter :: strle    = 64            ! short string lenght
    integer(kind=si), public, parameter :: strlen   = 256           ! medium string lenght
    integer(kind=si), public, parameter :: xstrlen  = 1024          ! long string lenght
    ! Endianess:
    ! Byte order depends on the platform where this file is being written. Intel CPUs (most commodity laptops and
    ! desktops) use little endian byte order; PowerPC CPUs (older Macintosh machines, IBM clusters and supercomputers)
    ! use big endian.
    character(len=6), public :: endianess = 'Little'            ! By default, it use BigEndian, can be switch to Little


    ! Mathematical Constants
    ! -------------------------------------------------------------------------80
    real (kind=dr), parameter :: APiCons                    = 3.141592653589793  ! () Archimedes-Pi constant
    real (kind=dr), parameter :: EN                         = 2.718281828459045  ! () Euler-Napier constant
    ! real (kind=dr), parameter :: First_Feigenbaum_constant = 2.502907875095892 ! () for choas and fractal
    ! real (kind=dr), parameter :: Apery_constant           = 1.202056903159594  ! () Riemann zeta function of 3
    ! real (kind=dr), parameter :: Omega_constant           = 0.567143290409783  ! () Omega*EN^(Omega)=1 
    ! real (kind=dr), parameter :: Golden_ratio             = 1.618033988749894  ! () (1+sqrt(5))/2

    ! Physical Constants
    ! -------------------------------------------------------------------------80
    ! real (kind=sr), parameter :: SPEED_OF_LIGHT            = 2.99792458E+08    ! (m/s)
    ! real (kind=sr), parameter :: PLANCKS_CONSTANT          = 6.62607015E-34    ! (J s)
    ! real (kind=sr), parameter :: GRAVITATIONAL_CONSTANT    = 6.67430E-11       ! (m3/kg/s2)
    ! real (kind=sr), parameter :: ELECTRON_MASS             = 9.1093837015E-31  ! (kg)
    ! real (kind=sr), parameter :: ELECTRON_CHARGE           = 1.602176634E-19   ! (s a)
    ! real (kind=sr), parameter :: PROTON_MASS               = 1.67262192369E-27 ! (kg)
    ! real (kind=sr), parameter :: MAGNETIC_PERMEABILITY     = 1.25663706212E-06 ! (s4 A2/kg/m3)
    ! real (kind=sr), parameter :: AVOGADRO_NUMBER           = 6.02214076E+23    ! (/mol)
    ! real (kind=sr), parameter :: BOLTZMANN_CONSTANT        = 1.380649E-23      ! (J/K)
    ! real (kind=sr), parameter :: STEFAN_BOLTZMANN_CONSTANT = 5.67E-08          ! (W/m2/K4)

    ! COMPUTED Constants
    ! -------------------------------------------------------------------------80
    ! real (kind=sr), parameter :: REGNAULT_IDEAL_GAS_CONSTANT = 8.314462618E+00 ! (J/mol/K) = N_A*k_B
    ! real (kind=sr), parameter :: ELECTRICAL_PERMITTIVITY   = 8.8541878128E-12  ! (F/m) = 1/(mu_0*c)
END MODULE PorPre
